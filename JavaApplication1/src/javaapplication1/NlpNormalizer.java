package javaapplication1;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RienCus on 24/10/2016.
 */
public class NlpNormalizer {
    
    public static int NBSP = 0x00A0; 
    private static final Pattern WHITESPACE_PATTERN = Pattern.compile("[\\p{Z}\\s]+");
    private static final Pattern REMOVE_ACCENT_PATTERN = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");  
  
    
    public String normalize(String text){
        // lowercase everything
        text = text.toLowerCase();
        
        text = unicodeTrim(text);
        
        text = text.replaceAll("['-=`~!@#$%^&*()_+{}|\\[\\]:;<,>\\.\\?\\/\\“\"]", "");
       
        text = Normalizer.normalize(text, Normalizer.Form.NFD);
	
	// replace all multiple whitespaces by a single space
	Matcher matcher = WHITESPACE_PATTERN.matcher(text);
        text = matcher.replaceAll("");

	// turn accented characters into normalized form. Turns &ouml; into o"
	
        
       
	
	// removes the marks found in the previous line.
	text = REMOVE_ACCENT_PATTERN.matcher(text).replaceAll("");
	
        
	
	return text;
        //return text.toLowerCase();
    }
    
    private static boolean isHtmlWhitespace(int c) { 
       return c == NBSP || Character.isWhitespace(c); 
   }  
  
    public  String unicodeTrim(String text) { 
       int leadingWhitespaceCount = 0; 
       int trailingWhitespaceCount = 0; 
       for (int i = 0; i < text.length(); i++) { 
           if (! isHtmlWhitespace(text.charAt(i))) { 
               break; 
           } 
           leadingWhitespaceCount++; 
       }  
       for (int i = (text.length() - 1); i > leadingWhitespaceCount; i--) { 
           if (! isHtmlWhitespace(text.charAt(i))) { 
               break; 
           } 
           trailingWhitespaceCount++; 
       } 
       if (leadingWhitespaceCount > 0 || trailingWhitespaceCount > 0) { 
           text = text.substring(leadingWhitespaceCount, text.length() - trailingWhitespaceCount); 
       } 
       return text; 
   } 
}
