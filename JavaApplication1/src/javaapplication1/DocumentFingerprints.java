/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.ArrayList;

/**
 *
 * @author LRSDA
 */
public class DocumentFingerprints {
    private ArrayList<Integer> docFp;
    private String docID;

    public DocumentFingerprints(String docName) {
        docFp = new ArrayList<>();
        docID = docName;
    }
    
    public ArrayList<Integer> getdocfp()
    {
        return docFp;
    }
    
    public int getlast()
    {
        return docFp.get(docFp.size()-1);
    }
    
    public void Addfp(int fp)
    {
        docFp.add(fp);
    }
    
    public int getfp(int i)
    {
        return docFp.get(i);
    }
    
    public String getID()
    {
        return docID;
    }
    
    public int getsize()
    {
        return docFp.size();
    }
}
