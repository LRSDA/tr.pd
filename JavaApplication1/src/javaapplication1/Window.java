package javaapplication1;

import java.util.ArrayList;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LRSDA
 */
public class Window {
    private ArrayList<Integer> hashVal;

    public Window() 
    {
        hashVal = new ArrayList<>();
    }
    
    public Window(ArrayList<Integer> hash) 
    {
        hashVal = hash;
    }
    
    public int getHashIndex(int i)
    {
        return hashVal.get(i);
    }
    
    public void addHash(int i)
    {
        hashVal.add(i);
    }
    
    public int getMinimumHash()
    {
        int minimum = (int) hashVal.get(0);
        for (int x: hashVal) {
            if (x < minimum )
            {
                minimum = x;
            }
        }
        return minimum;
    }
    
    public int getMinimumPosition()
    {
        int minimum = 0;
        for (int i= 1; i<hashVal.size(); i++) {
            if ( hashVal.get(i) < hashVal.get(minimum) )
            {
                minimum = i;
            }
        }
        return minimum;
    }
    
    public int getWindowSize()
    {
        return hashVal.size();
    }
    
    
    
    
}
