package javaapplication1;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by RienCus on 24/10/2016.
 */
public class NlpTokenizer {


    
    private String text = null;
    private List<String> token = null;

    public void setNewText(String text) {
        this.text = text;
        this.token = null;
    }

    public void tokenize() {
        List<String> token = new ArrayList<String>(Arrays.asList(this.text.split("\\s+")));
        List<String> removeList = new ArrayList<>();
        int check;
        for(String tkn: token)
        {
            check = NumberUtils.toInt(tkn);
            if(check!=0)
            {
                removeList.add(tkn);
            }
        }

        for(String removedToken: removeList) {
            token.remove(removedToken);
        }

        this.token = token;
    }

    public void addTokenToList(Map<String, Integer> tokens) {
        for(String s: this.getToken()) {
            if(!tokens.containsKey(s)) {
                tokens.put(s, 1);
            } else {
                Integer integer = tokens.get(s);
                tokens.put(s, integer+1);
            }
        }
    }
    
    public List<String> getToken() {
        return token;
    }
}
