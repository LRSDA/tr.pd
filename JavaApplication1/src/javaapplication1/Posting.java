/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author LRSDA
 */
public class Posting {
    private ArrayList<String> post;

    public Posting() {
        post = new ArrayList<>();
    }

    /**
     * @return the pl
     */
    public ArrayList<String> getPost() {
        return post;
    }

    /**
     * @param pl the pl to set
     */
    public void setPost(ArrayList<String> pl) {
        this.post = pl;
    }
    
    public void add(String x)
    {
        post.add(x);
    }
    
    public String get(int i)
    {
        return post.get(i);
    }
    
    public int size()
    {
        return post.size();
    }
    
    public void sortPost()
    {
        Collections.sort(post);
    }
}
