/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.ArrayList;

/**
 *
 * @author LRSDA
 */
public class PostingList {
    private ArrayList<Integer> dic;
    private ArrayList<Posting> postlist;

    public PostingList() {
        dic = new ArrayList<>();
        postlist = new ArrayList<>();
    }
    
    public boolean dictionaryExist(int x)
    {
        return dic.indexOf(x) != -1;
    }
    
    public void addPosting (String post,int  dictio)
    {
        int indexOfPost = dic.indexOf(dictio);
        postlist.get(indexOfPost).add(post);
    }
    
    public void addNew (String post,int  dictio)
    {
        dic.add(dictio);
        Posting temp = new Posting();
        temp.add(post);
        postlist.add(temp);
    }
    
    public int getDictionary (int i)
    {
        return dic.get(i);
    }
    
    public String getPost (int dictio, int i)
    {
        int indexPostlist = dic.indexOf(dictio);
        return postlist.get(indexPostlist).get(i);
    }
    
    public Posting getPosting (int dictio)
    {
        int indexPostlist = dic.indexOf(dictio);
        return postlist.get(indexPostlist);
    }
    
    public void sortPostingList()
    {
        for(Posting x: postlist)
        {
            x.sortPost();
        }
    }
    
    public int size()
    {
        return dic.size();
    }
    
}
