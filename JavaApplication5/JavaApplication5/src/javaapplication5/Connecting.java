package javaapplication5;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Windows
 */
public class Connecting {
    private static String url = null;
    

    
    private static void executeQuery(String sql,Connection con){
        try {
            Statement stmt = con.createStatement();
            stmt.execute(sql);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    private static ResultSet resultQuery(String sql,Connection con){
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            return rs;
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public static void createNewDatabase(){
        Connection con = connect();
        String sql ="CREATE TABLE IF NOT EXISTS CF (\n" +
                                "    token       LONGTEXT      NOT NULL \n" +
                                ");";
        executeQuery(sql, con);
    }
    
    public static void addDoc(ArrayList<String> namaDoc,Connection con){
        
        String sql="ALTER TABLE cf \n";
        for(int i=0;i<namaDoc.size() ;i++){
            sql+="ADD "+namaDoc.get(i)+" TEXT NOT NULL ";
            if(i!= namaDoc.size()-1){
                sql+=", \n";
            }
        }
        executeQuery(sql, con);
    }
    
    public static void addData(String kolom, ArrayList<String> kata, ArrayList<String> Data,Connection con)
    {
        String sql = "INSERT INTO cf (token,"+kolom+")"+
                "VALUES \n";
        for(int i = 0; i<kata.size() ;i++){
            sql+="("+kata.get(i)+","+Data.get(i)+")";
            if(i < kata.size()-1){
                sql+=", \n";
            }
            
            
        }
       
        sql+="; \n";
            
        System.out.println(sql);
        executeQuery(sql, con);
        System.out.println("Finish");
    }
    
    public static void checkDB(String namaDoc){
        Connection con = connect();
        String sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '"+namaDoc+"'";
        ResultSet rs = resultQuery(sql, con);
        Boolean isExist = false;
        try {
            while(rs.next()){
                if(rs.getString(1).equalsIgnoreCase(namaDoc))
                {
                    isExist = true;
                    System.out.println("DB exist");
                }
                    
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connecting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static boolean checkTable()
    {
        String sql = "SELECT 1 FROM CF LIMIT 1;";
        boolean result=false;
        try (Connection con = connect();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(sql);)
        {
            System.out.println("Table Exist");
            executeQuery("DROP TABLE IF EXISTS cf", con);
            System.out.println("Table Droped");
            createNewDatabase();
            System.out.println("Table Created");
            //droptable
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("tralala");
            createNewDatabase();
        }
        return result;
    }
        
        
    public static Connection connect(){
	System.out.println("-------- MySQL JDBC Connection Testing ------------");

        try {
            return DriverManager.getConnection( "jdbc:mysql://localhost:3306/spimi",
                    "root","");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return null;
    }
    
    public static void saveIt(Dictionary dictio){
        Connection con = connect();
        System.out.println(dictio.getDocumentSize());
        
        ArrayList<String> docs = new ArrayList();
        for(int i =0 ; i<dictio.getDocumentSize();i++){
            docs.add("Document"+i);
        }
        
        addDoc(docs, con);
        
        ArrayList<String> kata = new ArrayList();
        ArrayList<String> data = new ArrayList();
        String kolom = "";
        String tempdata = "";
        String tempkata = "";
        for(int j =0 ; j<dictio.getDocumentSize();j++){
            kolom += "Document"+j;
            if(j!= dictio.getDocumentSize()-1){
                kolom+=",";
            }
        }
        for(int i = 0; i<dictio.getWordsSize();i++){
            tempkata = "'"+dictio.getWord(i)+"'";
            tempdata = "";
            for(int j =0 ; j<dictio.getDocumentSize();j++){

                tempdata+="'"+dictio.getDocument(j).getToken(i).getTotal()+": ";
                for(int k =0;k<dictio.getDocument(j).getToken(i).getPosition().size();k++){
                    tempdata+= dictio.getDocument(j).getToken(i).getPosition().get(k);
                    if(k!= dictio.getDocument(j).getToken(i).getPosition().size()-1){
                        tempdata+=", ";
                    }
                }
                tempdata+="'";
                
                if(j!= dictio.getDocumentSize()-1){
                    tempdata+=",";
                }

            }
            kata.add(tempkata);
            data.add(tempdata);
            //addData(kolom, kata, data,con);
            System.out.println(kolom);
            System.out.println(tempkata);
            System.out.println(tempdata);
            System.out.println(i);
        }
        
        addData(kolom, kata, data,con);
        
    }
}
