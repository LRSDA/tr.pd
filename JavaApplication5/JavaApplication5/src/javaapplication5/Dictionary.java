/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;

import java.util.ArrayList;

/**
 *
 * @author Cyndy Alisia
 */
public class Dictionary {
    private ArrayList<Document> doc;
    private ArrayList<String> words;

    Dictionary(){
        this.doc = new ArrayList();
        this.words = new ArrayList();
    }

    public int getWordIndex(String word) {
        return this.words.indexOf(word);
    }

    public void addWords(String words) {
        this.words.add(words);
    }
    
    public int getWordsSize() {
        return this.words.size();
    }
    
    public String getWord(int index) {
        return this.words.get(index);
    }
    
    public ArrayList<String> getAllWords() {
        return this.words;
    }

    public Document getDocument(int index){
        return this.doc.get(index);
    }
    
    public void addDoc(Document doc) {
        this.doc.add(doc);
    }
    
    public int getDocumentSize(){
        return this.doc.size();
    }
    
    public void sortIt(){
        words.sort((w1,w2)->w1.compareTo(w2));
        doc.forEach((d) -> {
            d.sortToken();
        });
    }
}
