package javaapplication5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;

public class Spimi extends javax.swing.JFrame {
    
    public static int NBSP = 0x00A0; 
    private static final Pattern WHITESPACE_PATTERN = Pattern.compile("[\\p{Z}\\s]+");
    private static final Pattern REMOVE_ACCENT_PATTERN = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");  
  
    

    private File folder;
    private Pdf2Text pdf2Text;
    private StringTokenizer tokenisasi;
    private ArrayList<String> token;
    private ArrayList<String> stopword;
    private Dictionary dictionary;
    
    
    public Spimi() {
        this.token = new ArrayList();
        this.stopword = new ArrayList();
        this.dictionary = new Dictionary();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        LblText = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        btnRun = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Path   :");

        LblText.setText("...");

        jButton1.setText("Browse");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnRun.setText("Run");
        btnRun.setEnabled(false);
        btnRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRunActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblText, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 197, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRun)
                    .addComponent(jButton1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(LblText, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRun)
                .addContainerGap(237, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser jf = new JFileChooser();
        jf.setCurrentDirectory(new java.io.File("."));
        jf.setDialogTitle("choosertitle");
        jf.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY);
        jf.setAcceptAllFileFilterUsed(false);
        if (jf.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            LblText.setText(jf.getSelectedFile().toString());
            folder = new File(jf.getSelectedFile().toString());
            btnRun.setEnabled(true);
        } else {
            System.out.println("No Selection ");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRunActionPerformed
        File [] listFile = folder.listFiles();
        
        CreateStopword();
        
        for(int i = 0; i<listFile.length; i++){
            String ext =listFile[i].getName();
            String ext1=ext.substring(ext.lastIndexOf(".")+1);
            System.out.println(ext);
            dictionary.addDoc(new Document(ext));
            if(ext1.equals("pdf")){
                pdf2Text=new Pdf2Text();
                String fileContent=pdf2Text.getTextFromPDF(listFile[i]);
                fileContent=fileContent.toLowerCase();

                String petrn="^A-Za-z0-9 ";
                fileContent=fileContent.replaceAll("["+petrn+"]", "");
                fileContent=fileContent.replaceAll("\uFFFD", "");
                System.out.println(fileContent);
                
                token.clear();
                tokenisasi = new StringTokenizer(fileContent);
                while(tokenisasi.hasMoreTokens()){
                    String temp = tokenisasi.nextToken();
                    if (!inStopword(temp)){
                        token.add(temp);
                    } 
                }
                
                if(i != 0){
                    for (String word: dictionary.getAllWords()) {
                        dictionary.getDocument(i).addToken(new Token(word));
                    }
                }
                                
                for (int k = 0; k < token.size(); k++) {
                    System.out.println(token.get(k));
                    if(!dictionary.getAllWords().contains(token.get(k))){
                        dictionary.addWords(token.get(k));
                    }
                    if(dictionary.getDocument(i).getToken(token.get(k)) == null){
                        dictionary.getDocument(i).addToken(new Token(token.get(k)));
                        dictionary.getDocument(i).getToken(token.get(k)).addPosition(k);
                        dictionary.getDocument(i).getToken(token.get(k)).incrementTotal();
                    }
                    else{
                        dictionary.getDocument(i).getToken(token.get(k)).addPosition(k);
                        dictionary.getDocument(i).getToken(token.get(k)).incrementTotal();
                    }
		}      
                
            }
            
        }
        
        for(int i = 0; i<listFile.length; i++){
            for(int j = dictionary.getDocument(i).getTokenSize(); j < dictionary.getWordsSize(); j++){
                dictionary.getDocument(i).addToken(new Token(dictionary.getWord(j)));
            }
        }
        
        for(int i = 0; i<listFile.length; i++){
            System.out.println(dictionary.getDocument(i).getTokenSize());
        }
        
        dictionary.sortIt(); 
        dictionary.getAllWords().forEach((x) -> {
            System.out.println(x);
        });
        Connecting.saveIt(dictionary);
    }//GEN-LAST:event_btnRunActionPerformed

    public void CreateStopword()
    {
        try
            {
                BufferedReader in = new BufferedReader(new FileReader("stopword_mini.txt"));
                List<String> sw = new ArrayList<>();

                String line;
                while((line = in.readLine()) != null)
                {
                    tokenisasi = new StringTokenizer(line);
                    while(tokenisasi.hasMoreTokens()){
                        stopword.add(tokenisasi.nextToken());
                    }
                }
            } catch (FileNotFoundException ex) {
                System.out.println("Stopword Not found");
            } catch (IOException ex) {
            Logger.getLogger(Spimi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean inStopword(String token){
        return stopword.contains(token);
    }
    

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Spimi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Spimi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Spimi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Spimi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Spimi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblText;
    private javax.swing.JButton btnRun;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
