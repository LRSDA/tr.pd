/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;

import java.util.ArrayList;

/**
 *
 * @author Cyndy Alisia
 */
public class Token {
    private String name;
    private int total = 0;
    private ArrayList<Integer> position;

    Token(String Name){
        this.position = new ArrayList();
        this.name = Name;        
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void incrementTotal() {
        this.total++;
    }

    
    public ArrayList<Integer> getPosition() {
        return position;
    }

    public void setPosition(int position) {
        //this.position = position;
    }
    
    public void addPosition(int position){
        this.position.add(position);
    }
    
}
