package javaapplication5;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;

/**
 * Created by RienCus on 24/10/2016.
 */
public class Pdf2Text {
    public String getTextFromPDF(File file) {
        String text = null;
        try {
            PDDocument pdDocument = PDDocument.load(file);
            text = new PDFTextStripper().getText(pdDocument);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }

}
