/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;

import java.util.ArrayList;

/**
 *
 * @author Cyndy Alisia
 */
public class Document{
    private String docName;
    private ArrayList<Token> token = new ArrayList();

    public Document(String docName) {
        this.docName = docName;
    }

    public Document() {
    }
    
    public String getDocName() {
        return docName;
    }

    public void setDocName(String DocName) {
        this.docName = DocName;
    }
    
    public void addToken(Token token) {
        this.token.add(token);
    }
    
    public Token getToken(int index){
        return this.token.get(index);
    }
    
    public Token getToken(String word){
        
        for(Token t: this.token){
            if(t.getName() != null && word != null && t.getName().equals(word)){
                
                return t;
            }
        }
        return null;
    }
    
    public int getTokenIndex(Token token){
        int tamp = 0;
        
        for(Token t: this.token){
            if(t.getName() == null ? token.getName() == null : t.getName().equals(token.getName())){
                break;
            }
            tamp++;
        }
        return tamp;
    }
    
    public int getTokenSize(){
        return this.token.size();
    }
    
    public ArrayList<Token> getAllTokens() {
        return this.token;
    }
    
    public void sortToken(){
        token.sort((t1, t2) -> t1.getName().compareTo(t2.getName()));
    }
}
